package com.team16.BTCL;



import android.app.Activity;
import android.graphics.Color;
import com.androidplot.xy.*;

 
/**
 * 
 * Class that manage the diagram generation
 *
 */
public class DiagramDesign
{

    
    private static final int HISTORY_SIZE = 30;            // number of points to plot in history
    private XYPlot HistoryPlot = null;
    private SimpleXYSeries Series = null;


    /** Called when the activity is first created. */
    public DiagramDesign(Activity act) {
        
        // setup the History plot:
        HistoryPlot = (XYPlot) act.findViewById(R.id.diagram);

        Series = new SimpleXYSeries("CPU Usage");
        Series.useImplicitXVals();
        

        HistoryPlot.setRangeBoundaries(0, 100, BoundaryMode.FIXED);
        HistoryPlot.setDomainBoundaries(0, HISTORY_SIZE, BoundaryMode.FIXED);
        HistoryPlot.addSeries(Series, new LineAndPointFormatter(Color.rgb(100, 100, 200), Color.BLACK,null));
        HistoryPlot.setDomainStepValue(11);
        HistoryPlot.setTicksPerRangeLabel(1);
        HistoryPlot.setDomainLabel("Sample Index");
        HistoryPlot.getDomainLabelWidget().pack();
        HistoryPlot.setRangeLabel("Perc. [%]");
        HistoryPlot.getRangeLabelWidget().pack();
    }

    
    /**
     * function called for the updating of the plot with the new value
     * @param str string that represent the new value that we need to plot
     */
    public synchronized void onDataChanged(String str) {
    	
    	float value = Float.parseFloat(str);
        // get rid the oldest sample in history:
        if (Series.size() > HISTORY_SIZE) {
            Series.removeFirst();
        }
        //System.out.println("value: "+ value + " str: "+str);
        // add the latest history sample:
        Series.addLast(null, value);
    	//System.out.println("value: "+ value + " str: "+str);
        // redraw the Plots:
        HistoryPlot.redraw();
    }
}